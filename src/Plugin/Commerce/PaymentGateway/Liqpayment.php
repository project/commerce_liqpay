<?php

namespace Drupal\commerce_liqpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_liqpay\Controller\LiqPayStatusRequestSender;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Off-site Redirect payment Liqapy gateway.
 *
 * @CommercePaymentGateway(
 *   id = "liqpay_offsite_gateway",
 *   label = "LIQPAY Offsite Payment Gateway",
 *   display_label = "Liqpay",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_liqpay\PluginForm\OffsiteRedirect\LiqpaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Liqpayment extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {

  use StringTranslationTrait;


  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new Liqpayment object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory service.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface|null $minor_units_converter
   *   The minor units converter.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    ConfigFactoryInterface $configFactory,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerFactory,
    MinorUnitsConverterInterface $minor_units_converter = NULL,
  ) {
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
    $this->loggerFactory = $loggerFactory;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('logger.factory'),
      $container->get('commerce_price.minor_units_converter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config   = $this->configFactory->get('commerce_liqpay.settings');
    $defaults = [];

    foreach ($this->commerceLiqpayTransactionStatuses() as $status_name => $status) {
      $defaults['message_' . $status_name] = $status['message'];
    }

    $defaults = $defaults + [
      'version'     => $config->get('commerce_liqpay.version'),
      'public_key'  => '',
      'private_key' => '',
      'action_url'  => $config->get('commerce_liqpay.checkout_url'),
      'action'      => $config->get('commerce_liqpay.action'),
      'api_url'     => $config->get('commerce_liqpay.api_url'),
      'server_notifications'     => $config->get('commerce_liqpay.server_notifications'),
      'description' => 'Ordered # [commerce_order:order_id] at [site:name].',
    ];
    return $defaults + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['version'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API version'),
      '#default_value' => $this->configuration['version'],
      '#maxlength'     => 1,
      '#required'      => TRUE,
    ];

    $form['public_key'] = [
      '#title'         => $this->t('Public key'),
      '#description'   => $this->t('Key is the unique store identifier. You can get the key in @link.', [
        '@link' => Link::fromTextAndUrl($this->t('the store settings'), Url::fromUri('https://www.liqpay.ua/en/admin/business'))->toString(),
      ]),
      '#default_value' => $this->configuration['public_key'],
      '#type'          => 'textfield',
      '#required'      => TRUE,
    ];

    $form['private_key'] = [
      '#title'         => $this->t('Private key'),
      '#description'   => $this->t('Key is the unique store identifier. You can get the key in @link.', [
        '@link' => Link::fromTextAndUrl($this->t('the store settings'), Url::fromUri('https://www.liqpay.ua/en/admin/business'))->toString(),
      ]),
      '#default_value' => $this->configuration['private_key'],
      '#type'          => 'textfield',
      '#required'      => TRUE,
    ];

    $form['action'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Action'),
      '#default_value' => $this->configuration['action'],
      '#required'      => TRUE,
    ];

    $form['action_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Action url'),
      '#description'   => $this->t('Payment Action url required by Liqpay.'),
      '#default_value' => $this->configuration['action_url'],
      '#required'      => TRUE,
    ];

    $form['server_notifications'] = [
      '#title'         => $this->t('Enable notifications from Liqpay API'),
      '#description'   => $this->t('Notification about change of payment status (server-> server)'),
      '#default_value' => $this->configuration['server_notifications'],
      '#type'          => 'checkbox',
    ];

    $form['api_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API url'),
      '#description'   => $this->t('Payment API url required by Liqpay. This API url needed to verify payment status.'),
      '#default_value' => $this->configuration['api_url'],
      '#required'      => TRUE,
    ];

    $form['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Payment description'),
      '#description'   => $this->t('Payment description required by Liqpay. This description will be available in LiqPay interface. You can use token replacement patterns for adding order id, site name, etc.'),
      '#default_value' => $this->configuration['description'],
      '#required'      => TRUE,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['token_help'] = [
        '#type'  => 'details',
        '#title' => $this->t('Available tokens'),
      ];

      $form['token_help']['token_tree_link'] = [
        '#theme'       => 'token_tree_link',
        '#token_types' => ['commerce_order'],
      ];
    }

    $form['messages'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Messages'),
      '#description' => $this->t('You can find more details about LiqPay statuses at @link.', [
        '@link' => Link::fromTextAndUrl($this->t('API reference'), Url::fromUri('https://www.liqpay.ua/en/doc/checkout'))->toString(),
      ]),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];

    foreach ($this->commerceLiqpayTransactionStatuses() as $status_name => $status) {
      $form['messages']['message_' . $status_name] = [
        '#type'          => 'textarea',
        '#rows'          => 2,
        '#title'         => $status['title'],
        '#description'   => $this->t('API status machine name: @status', [
          '@status' => $status_name,
        ]),
        '#default_value' => $this->configuration['message_' . $status_name] === '' ? $status['message'] : $this->configuration['message_' . $status_name],

      ];
    }

    return $form;
  }

  /**
   * Returns an array of all handled LiqPay transaction statuses.
   *
   * See https://www.liqpay.ua/en/documentation/api/callback for details.
   *
   * @param string $name
   *   Status name.
   *
   * @return array|bool
   *   FALSE on error validation array otherwise.
   */
  public function commerceLiqpayTransactionStatuses($name = NULL) {
    $statuses = [
      // Final payment statuses.
      'error' => [
        'status'  => 'error',
        'title'   => $this->t('Failed payment (error)'),
        'message' => $this->t('Failed payment. Data is incorrect.'),
      ],
      'failure' => [
        'status'  => 'failure',
        'title'   => $this->t('Failed payment (failure)'),
        'message' => $this->t('Failed payment.'),
      ],
      'reversed' => [
        'status'  => 'reversed',
        'title'   => $this->t('Payment refunded'),
        'message' => $this->t('Payment amount refunded.'),
      ],
      'subscribed' => [
        'status'  => 'subscribed',
        'title'   => $this->t('Subscribed'),
        'message' => $this->t('Subscribed successfully framed.'),
      ],
      'unsubscribed' => [
        'status'  => 'unsubscribed',
        'title'   => $this->t('Unsubscribed'),
        'message' => $this->t('Subscribed successfully deactivated.'),
      ],
      'success' => [
        'status'  => 'success',
        'title'   => $this->t('Success'),
        'message' => $this->t('Successful payment.'),
      ],

      // Statuses that required payment confirmation.
      '3ds_verify' => [
        'status'  => '3ds_verify',
        'title'   => $this->t('3DS verification is required'),
        'message' => $this->t('To finish the payment you will require a 3ds_verify.'),
      ],
      'captcha_verify' => [
        'status'  => 'captcha_verify',
        'title'   => $this->t('Captcha verification is required'),
        'message' => $this->t('Waiting for customer to confirm with captcha.'),
      ],
      'cvv_verify' => [
        'status'  => 'cvv_verify',
        'title'   => $this->t("Sender's card CVV is required"),
        'message' => $this->t('To finish the payment you will require a cvv_verify.'),
      ],
      'ivr_verify' => [
        'status'  => 'ivr_verify',
        'title'   => $this->t("IVR verification is required"),
        'message' => $this->t('Waiting for customer to confirm with IVR.'),
      ],
      'otp_verify' => [
        'status'  => 'otp_verify',
        'title'   => $this->t("OTP confirmation is required"),
        'message' => $this->t('OTP is sent to a customer phone number. To finish the payment it is required otp_verify.'),
      ],
      'password_verify' => [
        'status'  => 'password_verify',
        'title'   => $this->t("Password confirmation is required"),
        'message' => $this->t('Waiting for customer to confirm with Privat24.'),
      ],
      'phone_verify' => [
        'status'  => 'phone_verify',
        'title'   => $this->t("Phone verification is required"),
        'message' => $this->t('Waiting for customer to enter a phone number. To finish the payment you will require a phone_verify.'),
      ],
      'pin_verify' => [
        'status'  => 'pin_verify',
        'title'   => $this->t("PIN verification is required"),
        'message' => $this->t('Waiting for customer to confirm with PIN-code.'),
      ],
      'receiver_verify' => [
        'status'  => 'receiver_verify',
        'title'   => $this->t("Receiver additional verification is required"),
        'message' => $this->t('Receiver additional data is required. To finish the payment you will require a receiver_verify.'),
      ],
      'sender_verify' => [
        'status'  => 'sender_verify',
        'title'   => $this->t("Sender additional verification is required"),
        'message' => $this->t("Sender's additional data is required. To finish the payment you will require a sender_verify."),
      ],
      'senderapp_verify' => [
        'status'  => 'senderapp_verify',
        'title'   => $this->t("Sender application verification is required"),
        'message' => $this->t("Waiting for customer to confirm with Privat24."),
      ],
      'wait_qr' => [
        'status'  => 'wait_qr',
        'title'   => $this->t("QR-code verification is required"),
        'message' => $this->t("Waiting for customer to scan QR-code."),
      ],
      'wait_sender' => [
        'status'  => 'wait_sender',
        'title'   => $this->t("Mobile app verification is required"),
        'message' => $this->t("Waiting for customer to confirm in mob app Privat24/SENDER."),
      ],

      // Other payment statuses.
      'cash_wait' => [
        'status'  => 'cash_wait',
        'title'   => $this->t('Waiting for cash'),
        'message' => $this->t('Waiting for payment in self-service terminal.'),
      ],
      'hold_wait' => [
        'status'  => 'hold_wait',
        'title'   => $this->t('Amount blocked'),
        'message' => $this->t("Amount was successfully blocked on the sender's account."),
      ],
      'invoice_wait' => [
        'status'  => 'invoice_wait',
        'title'   => $this->t('Invoice created'),
        'message' => $this->t("Invoice is created successfully, waiting for a payment."),
      ],
      'prepared' => [
        'status'  => 'prepared',
        'title'   => $this->t('Invoice prepared'),
        'message' => $this->t("Payment is created, waiting for customer to finish it."),
      ],
      'processing' => [
        'status'  => 'processing',
        'title'   => $this->t('Processing'),
        'message' => $this->t('Payment is processing.'),
      ],
      'wait_accept' => [
        'status'  => 'wait_accept',
        'title'   => $this->t('Wait Accept'),
        'message' => $this->t('Money are withdrawn from client but the store is still not verified. If the store is not activated for 60 days, payment will be automatically cancelled.'),
      ],
      'wait_card' => [
        'status'  => 'wait_accept',
        'title'   => $this->t('Wait Card (compensation)'),
        'message' => $this->t("Recipient didn't set the compensation method."),
      ],
      'wait_compensation' => [
        'status'  => 'wait_compensation',
        'title'   => $this->t('Wait compensation'),
        'message' => $this->t("Payment is successful, it will be transferred in daily settlement."),
      ],
      'wait_lc' => [
        'status'  => 'wait_lc',
        'title'   => $this->t('Protected payment'),
        'message' => $this->t("Charging is successful, waiting for receipt of goods confirmation."),
      ],
      'wait_reserve' => [
        'status'  => 'wait_reserve',
        'title'   => $this->t('Refund reserved'),
        'message' => $this->t("Funds are reserved to make a refund according to a refund request."),
      ],
      'wait_secure' => [
        'status'  => 'wait_secure',
        'title'   => $this->t('Wait secure'),
        'message' => $this->t("Payment is verified."),
      ],

      // @todo Verify this status is still available.
      'sandbox' => [
        'status'  => 'sandbox',
        'title'   => $this->t('Sandbox'),
        'message' => $this->t('Transaction marked as sandbox.'),
      ],
    ];

    if (NULL !== $name) {
      if (isset($statuses[$name])) {
        return $statuses[$name];
      }
      return FALSE;
    }

    return $statuses;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $required_fields = [
      'public_key',
      'private_key',
      'description',
      'api_url',
      'action_url',
    ];
    foreach ($required_fields as $key) {
      if (empty($values[$key])) {
        $this->messenger()->addError($this->t('LiqPay service is not configured for use. Please contact an administrator to resolve this issue.'));
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values                                      = $form_state->getValue($form['#parents']);
      $this->configuration['version']              = $values['version'];
      $this->configuration['public_key']           = $values['public_key'];
      $this->configuration['private_key']          = $values['private_key'];
      $this->configuration['action']               = $values['action'];
      $this->configuration['action_url']           = $values['action_url'];
      $this->configuration['api_url']              = $values['api_url'];
      $this->configuration['description']          = $values['description'];
      $this->configuration['server_notifications'] = $values['server_notifications'];

      foreach ($values['messages'] as $message_name => $message) {
        $this->configuration[$message_name] = $message;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // Receiving transaction status.
    if (!$data = $this->receivingLiqpayTransaction($order)) {
      $this->messenger()->addError($this->t('Invalid Transaction. Please try again'));
      return $this->onCancel($order, $request);
    }
    else {
      $data = $this->receivingLiqpayTransaction($order);

      // Check that payment was successful.
      if (
        ($data['status'] != 'success')
        && ($data['status'] != 'sandbox')
      ) {
        $this->messenger()->addError($this->t('Invalid Transaction. Please try again'));
        throw new PaymentGatewayException($data['message']);
      }

      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment         = $payment_storage->create([
        'state'           => $data['status'],
        'amount'          => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id'        => $data['order_id'],
        'remote_id'       => $data['liqpay_order_id'],
        'remote_state'    => $data['status'],
      ]);
      $payment->save();
      $this->messenger()->addMessage($data['message']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
    throw new PaymentGatewayException('Payment cancelled by user');
  }

  /**
   * Getting configuration.
   *
   * @return array
   *   Configuration array.
   */
  public function getStoreDataConfiguration() {
    $config = $this->configFactory->get('commerce_liqpay.settings');

    return [
      'public_key'   => $this->configuration['public_key'] ?? '',
      'private_key'  => $this->configuration['private_key'] ?? '',
      'api_url'      => $this->configuration['api_url'] ?? $config->get('commerce_liqpay.api_url'),
      'checkout_url' => $this->configuration['checkout_url'] ?? $config->get('commerce_liqpay.checkout_url'),
      'version'      => $this->configuration['version'] ?? $config->get('commerce_liqpay.version'),
    ];
  }

  /**
   * Receiving Transaction for module payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Current order.
   *
   * @return array|bool
   *   FALSE on error validation array otherwise.
   */
  protected function receivingLiqpayTransaction(OrderInterface $order) {
    $store_data = $this->getStoreDataConfiguration();

    $liqpay = new LiqPayStatusRequestSender($store_data);

    // Check transaction status.
    $data = $liqpay->api("request", [
      'action'   => 'status',
      'version'  => $this->configuration['version'] ?? $store_data['version'],
      'order_id' => $order->id(),
    ]);

    if (!$data) {
      return FALSE;
    }

    $status = $this->commerceLiqpayTransactionStatuses($data['status']);
    if (isset($status) && FALSE === $status) {
      return FALSE;
    }

    // Check amount and currency.
    if (!isset($data['currency']) && !isset($data['amount'])) {
      return FALSE;
    }

    $transaction_currency = $liqpay->isSupportedCurrency($data['currency']) ? $data['currency'] : FALSE;
    $transaction_amount   = $data['amount'];
    $order_currency       = $order->getTotalPrice()->getCurrencyCode();
    $order_amount         = $order->getTotalPrice()->getNumber();

    if (!$this->validateCurrency($transaction_currency, $order_currency) || !$this->validateAmount($transaction_amount, $order_amount)) {
      return FALSE;
    }

    // Find out a message for this transaction.
    $message_liqpay = $this->configuration['message_' . $status['status']];
    if (!empty($message_liqpay)) {
      $data['message'] = $message_liqpay;
    }
    else {
      $data['message'] = $status['message'];
    }

    // Everything is ok with this transaction.
    return $data;
  }

  /**
   * Currency default validation.
   *
   * @param string $transaction_currency
   *   Currency from Liqpay.
   * @param string $order_currency
   *   Currency from order.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateCurrency($transaction_currency, $order_currency) {
    if ($transaction_currency != $order_currency) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Amount default validation.
   *
   * @param float $transaction_amount
   *   Amount from Liqpay.
   * @param float $order_amount
   *   Amount from order.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateAmount($transaction_amount, $order_amount) {
    if ($transaction_amount != $order_amount) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * IPN callback.
   *
   * IPN will be called after a succesful liqpay payment or changed state.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The response, or NULL to return an empty HTTP 200 response
   */
  public function onNotify(Request $request) {
    $liqpay_response_on_base_64 = $request->request->get('data');
    $liqpay_response_convert_to_json = base64_decode($liqpay_response_on_base_64);
    $liqpay_response_convert_to_array = Json::decode($liqpay_response_convert_to_json);

    $order = Order::load($liqpay_response_convert_to_array['order_id']);
    // Receiving transaction status.
    if (!$data = $this->receivingLiqpayTransaction($order)) {
      $this->messenger()->addError($this->t('Invalid Transaction. Please try again'));
      return $this->onCancel($order, $request);
    }
    else {
      $data = $this->receivingLiqpayTransaction($order);
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment         = $payment_storage->create([
      'state'           => $data['status'],
      'amount'          => $order->getTotalPrice(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id'        => $data['order_id'],
      'remote_id'       => $data['liqpay_order_id'],
      'remote_state'    => $data['status'],
    ]);
    $payment->save();
    $this->messenger()->addMessage($data['message']);
    $this->loggerFactory->get('commerce_payment')->notice('Liqpay response for order id: @order. Status: @status.',
      [
        '@order' => $data['order_id'],
        '@status' => $data['status'],
      ]);

    return new Response('Received new payment status', 200);
  }

}
